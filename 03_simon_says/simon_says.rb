def echo(message)
    message
end

def shout(message)
    message.upcase
end

def repeat(message, times=2)
    repeated = ""
    for i in 1..times do
        repeated = repeated + message + " "
    end
    return repeated.strip
end

def start_of_word(word, number_of_letters)
    return word[0..number_of_letters-1]
end

def first_word(string)
    return string.split[0]
end

def titleize(string)
    words = string.split
    little_words = ['a', 'an', 'the', 'or', 'and', 'in', 'of', 'in', 'to', 'but', 'over']
    words.each_with_index do |word, index|
        if (not little_words.include?(word)) || index == 0
            word[0] = word[0].upcase
        end
    end
    return words.join(" ")
end
