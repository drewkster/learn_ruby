class Timer
    attr_accessor :seconds

    def initialize()
        @seconds = 0
    end

    def time_string
        total_time = @seconds

        hours = total_time / 3600
        total_time = total_time % 3600

        minutes = total_time / 60
        total_time = total_time % 60

        return "#{pad(hours)}:#{pad(minutes)}:#{pad(total_time)}"        
    end

    def pad(number)
        if number < 10
            return "0#{number}"
        end

        return number
    end
end
