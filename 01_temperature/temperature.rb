def ftoc(fahrenheit)
    celsius = 5.0 * (fahrenheit - 32) / 9.0
    return celsius
end

def ctof(celsius)
    fahrenheit = 9.0 * celsius / 5.0 + 32
    return fahrenheit
end
