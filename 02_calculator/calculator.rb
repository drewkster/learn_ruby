def add(operand1, operand2)
    return operand1 + operand2
end

def subtract(operand1, operand2)
    return operand1 - operand2
end

def sum(array)
    sum = 0
    for number in array do
        sum += number
    end

    return sum
end

def multiply_two(operand1, operand2)
    return operand1 * operand2
end

def multiply_many(array)
    total = array[0]
    for number in array[1..array.length] do
        total *= number
    end
    return total
end

def power(base, exponent)
    return base ** exponent
end

def factorial(number)
    if number == 0
        return number
    end

    if number == 1
        return number
    end

    total = number
    total *= factorial(number - 1)

    return total
end
