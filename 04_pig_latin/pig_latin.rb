def translate(string)
    words = string.split
    vowels = ['a', 'e', 'i', 'o', 'u']
    words.each_with_index do |word, index|
        i = 0
        while not vowels.include?(word[i])
            word.insert(-1, word[i])
            if word[i] == 'q'
                i += 1
                word.insert(-1, word[i])
            end
            i += 1
        end
        word.insert(-1, 'ay')
        words[index] = word.slice(i..word.length)
    end

    return words.join(" ")
end
