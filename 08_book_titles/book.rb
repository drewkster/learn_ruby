class Book
    attr_accessor :title

    def title=(new_title)
        words = new_title.split
        exceptions = ['a', 'an', 'the', 'and', 'but', 'nor', 'or', 'to', 'for', 'by', 'on', 'in', 'at', 'from', 'of']
        words.each_with_index do |word, index|
            if (index == 0) || (not exceptions.include?(word))
                word[0] = word[0].upcase
            end
        end
        @title = words.join(" ")
    end

    def title
        @title
    end
end
